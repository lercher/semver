# semver - Manage Semantic Version Numbers for Go and HTML Sources

The command line tool `semver` can be included in build batches to automatically advance
semantic version numbers in go (golang) source codefiles, usually named `version.go` and html files.

## Supported file types

* `.go` - see [version.go](version.go)
* `.html` - see [version.html](version.html)
* `.json` - see [winres.json](winres.json)

This tool uses itself, so have a look at [version.go](version.go) to see what it looks like.

## Sample Command Lines

For a go file:

```txt
C:\Daten\Go\src\gitlab.com\lercher\semver>semver.exe -minor -f version.go
2018/10/15 16:38:17 This is semver.exe 1.0.3 (C) 2018 by M. Lercher
2018/10/15 16:38:17 const semverVersion = `1.0.3`
2018/10/15 16:38:17 const semverVersion = `1.1.4`
```

For an html file:

```txt
C:\Daten\Go\src\gitlab.com\lercher\semver>semver.exe -minor -f version.html
2018/10/15 16:38:22 This is semver.exe 1.0.3 (C) 2018 by M. Lercher
2018/10/15 16:38:22 <p id="version">1.0.3</p>
2018/10/15 16:38:22 <p id="version">1.1.4</p>
```

Example "git tag":

```txt
C:\git\src\gitlab.com\lercher\semver>go run . -wincmd git tag v{{.}}
2024/07/26 19:02:41 This is semver 1.4.9 (C) 2018-2024 by M. Lercher
2024/07/26 19:02:41 executing: [git tag v1.4.9]
2024/07/26 19:02:41 exit code 0
```

```txt
C:\git\src\gitlab.com\lercher\semver>go run . -wincmd git tag v{{.}} --delete
2024/07/26 19:03:41 This is semver 1.4.9 (C) 2018-2024 by M. Lercher
2024/07/26 19:03:41 executing: [git tag v1.4.9 --delete]
Deleted tag 'v1.4.9' (was 08c30fa)
2024/07/26 19:03:41 exit code 0
```

No file:

```text
C:\Daten\Go\src\gitlab.com\lercher\semver>semver -?
2024/07/26 19:00:56 This is semver 1.4.8 (C) 2018-2024 by M. Lercher
flag provided but not defined: -?
Usage of semver:
  -f file
        operate on this go or html file, i.e. parse it for a line containing 'Version = `...`' (go), 'id='version'...>...<'(html) and increment the `...` semantic version by the other flags given (default "version.go")
  -json string
        comma separated list of json string attribute names where the new version number as 'maj.min.0.patch' is set to, only valid if -winres is set (default "version,file_version,product_version,FileVersion,ProductVersion")
  -major
        increment major version
  -minor
        increment minor version
  -patch
        increment patch version
  -print
        print the loaded version, can't be combined with other options besides -f
  -wincmd
        all normal args are treated as Go templates to be submitted as a new command line, where {{.}} is replaced by the scanned version. -wincmd is only compatible with -f, example: 'git tag v{{.String}}'. dot supports Major, Minor, Patch uint64, Pre []*PRVersion, Build []string and String().
  -winres path
        path to a json file description of a windows resources containing RT_MANIFEST and RT_VERSION sections, see also -json
```

## JSON Support

Using the `-winres` and `-json` options you can also put the patched version number to a
JSON file, replacing all the attributes that are listed after `-json`.

It is intended to be used by a program to create a *.syso file from the `-winres` file
which finally gets compiled into a PE ressource section to show the version number
via Windows explorer properties like this:

```sh
semver -patch -f cmd/some/version.go -json version,file_version,FileVersion -winres winres/winres.json
go-winres make --arch amd64 --out cmd/some/rsrc
go build
```
