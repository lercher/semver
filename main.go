package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"
	"text/template"

	"gopkg.in/blang/semver.v1"
)

var (
	flagMinor  = flag.Bool("minor", false, "increment minor version")
	flagMajor  = flag.Bool("major", false, "increment major version")
	flagPatch  = flag.Bool("patch", false, "increment patch version")
	flagPrint  = flag.Bool("print", false, "print the loaded version, can't be combined with other options besides -f")
	flagCmd    = flag.Bool("wincmd", false, "all normal args are treated as Go templates to be submitted as a new command line, where {{.}} is replaced by the scanned version. -wincmd is only compatible with -f, example: 'git tag v{{.String}}'. dot supports Major, Minor, Patch uint64, Pre []*PRVersion, Build []string and String().")
	flagFile   = flag.String("f", "version.go", "operate on this go or html `file`, i.e. parse it for a line containing 'Version = `...`' (go), 'id='version'...>...<'(html) and increment the `...` semantic version by the other flags given")
	flagWinres = flag.String("winres", "", "`path` to a json file description of a windows resources containing RT_MANIFEST and RT_VERSION sections, see also -json")
	flagJSON   = flag.String("json", "version,file_version,product_version,FileVersion,ProductVersion", "comma separated list of json string attribute names where the new version number as 'maj.min.0.patch' is set to, only valid if -winres is set")
)

var readOnly = false

var locators = map[string]*regexp.Regexp{
	".go":   regexp.MustCompile("^(.*Version = `)(.+?)(`.*)$"),
	".html": regexp.MustCompile(`^(.*id=['"]version['"].*>)(.+?)(<.*)$`),
}

func main() {
	log.Println("This is", os.Args[0], semverVersion, "(C) 2018-2024 by M. Lercher")
	flag.Parse()

	if *flagCmd {
		if flag.NArg() == 0 {
			flag.Usage()
			fmt.Println("-wincmd must be combined with at least one non-flag parameter")
			os.Exit(2)
		}
	} else if (!*flagPatch && !*flagMinor && !*flagMajor && !*flagPrint) || flag.NArg() != 0 {
		flag.Usage()
		fmt.Println("must use -patch, -minor, -major or -print with no other non-flag parameter")
		os.Exit(2)
	}

	readOnly = *flagPrint || *flagCmd

	f, err := os.Open(*flagFile)
	if err != nil {
		log.Fatalln(err)
	}
	ext := filepath.Ext(*flagFile)
	re, ok := locators[ext]
	if !ok {
		log.Fatalln("unsupported file extension", ext)
	}
	sc := bufio.NewScanner(f)
	var lines []string
	var newVersion *semver.Version
	for sc.Scan() {
		line := sc.Text()
		if re.MatchString(line) {
			if !readOnly {
				log.Println(line)
			}
			matches := re.FindStringSubmatch(line)
			ver, err := semver.New(matches[2])
			if err != nil {
				log.Fatalln("can't parse", matches[2], "as a semantic version:", err)
			}
			change(ver)
			line = matches[1] + ver.String() + matches[3]
			if !readOnly {
				log.Println(line)
			}
			newVersion = ver
		}
		lines = append(lines, line)
	}
	f.Close()
	if readOnly {
		os.Exit(0)
	}

	f, err = os.Create(*flagFile)
	if err != nil {
		log.Fatalln(err)
	}
	for i, line := range lines {
		_, err = fmt.Fprintln(f, line)
		if err != nil {
			log.Fatalln("line", i+1, err)
		}
	}
	err = f.Close()
	if err != nil {
		log.Fatalln(err)
	}

	if *flagWinres != "" {
		err = patchWinres(*flagWinres, *flagJSON, newVersion)
		if err != nil {
			log.Fatalln(err)
		}
	}
}

func patchWinres(fn, attribList string, newVersion *semver.Version) error {
	ver := fmt.Sprint(newVersion.Major, ".", newVersion.Minor, ".0.", newVersion.Patch)
	log.Println("patching", fn, "to", ver, "...")

	list := strings.Split(attribList, ",")
	m := make(map[string]bool, len(list))
	for _, attrib := range list {
		m[attrib] = true
	}

	bs, err := os.ReadFile(fn)
	if err != nil {
		return err
	}

	winres := make(map[string]any)
	err = json.Unmarshal(bs, &winres)
	if err != nil {
		return fmt.Errorf("load %v: %v", fn, err)
	}

	stack := make([]string, 0, 8)
	walk(winres, stack, m, ver)

	newWinres, err := json.MarshalIndent(winres, "", "  ")
	if err != nil {
		return fmt.Errorf("encode %v: %v", fn, err)
	}

	err = os.WriteFile(fn, newWinres, 0644)
	if err != nil {
		return err
	}

	return nil
}

func walk(tree map[string]any, stack []string, m map[string]bool, ver string) {
	for k, v := range tree {
		stack = append(stack, k)
		switch val := v.(type) {
		case string:
			if m[k] {
				log.Println(val, "->", ver, stack)
				tree[k] = ver
			}

		case bool, []any:
			// not applicable

		case map[string]any:
			walk(val, stack, m, ver)

		default:
			log.Printf("ignoring value at %v of type %T = %v", stack, v, v)
		}
		stack = stack[:len(stack)-1]
	}
}

func change(sv *semver.Version) {
	if *flagCmd {
		buf := new(bytes.Buffer)
		args := make([]string, 0, flag.NArg()+1)
		for _, arg := range flag.Args() {
			tpl, err := template.New("cmd").Parse(arg)
			if err != nil {
				log.Fatalln("-wincmd:", err)
			}
			err = tpl.Execute(buf, sv)
			if err != nil {
				log.Fatalln("-wincmd:", err)
			}
			args = append(args, buf.String())
			buf.Reset()
		}
		log.Println("executing:", args)
		cmd := exec.Command(args[0], args[1:]...)
		cmd.Stderr = os.Stderr
		cmd.Stdin = os.Stdin
		cmd.Stdout = os.Stdout
		err := cmd.Run()
		if err != nil {
			log.Fatalln("-wincmd:", err)
		}
		log.Println("exit code", cmd.ProcessState.ExitCode())
		return
	}
	if *flagPrint {
		fmt.Print(sv.String())
		return
	}
	if *flagMajor && !*flagMinor {
		sv.Major++
		sv.Minor = 0
	}
	if *flagMajor && *flagMinor {
		sv.Major++
		sv.Minor++
	}
	if !*flagMajor && *flagMinor {
		sv.Minor++
	}
	sv.Patch++
}
